import { Component } from '@angular/core'
import { lorem } from 'faker'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = '|_ typing'

	template: { index: number, char: string, color: string }[][] = lorem
		.sentence()
		.split(' ')
		.map((word, i) =>
			[...word].map((char, i) => ({
				index: i,
				char: char,
				color: 'grey'
			})
		))

	templateString: string = this.template.map(
		word => word.map(char => char.char).join('')
	).join(' ')

	typingMode: string = 'practise'
	mistakes: number = 0
	showMistakes: boolean = false
	showSuccess: boolean = false

	onTyping(value: string) {

		const index = value.length-1

	/* if user trys to copy paste faulty text
		const correct = this.templateString.startsWith(value)

		if (!correct) {
			const allMistakes = [...value].filter((x, i) => x !== this.templateString[i])
			console.log('all mistakes', allMistakes)
		} */

		// get current word
		const truncSentence = this.templateString.slice(0, index)
		const senIndex = truncSentence.split(' ').length-1
		const wordIndex = index - (~truncSentence.lastIndexOf(' ') ? truncSentence.lastIndexOf(' ')+1 : 0)

		// set color if correct or wrong
		if (this.templateString[index] == value[index]) {

			// dont set color to red if whitespace
			if (this.templateString[index] !== ' ') {

				this.template[senIndex][wordIndex].color = 'green'
			}

		} else {

			// counts mistakes if in challenge mode
			if (this.typingMode == 'challenge')
				this.countMistakes()

			// dont set color to red if whitespace
			if (this.templateString[index] !== ' ') {
				this.template[senIndex][wordIndex].color = 'red'
			}
		}

		// if done show mistakses or success 
		if (this.templateString.length == index+1 && this.templateString === value) {
			if (this.typingMode == 'practise' || this.mistakes == 0) {
				this.showSuccess = true
				this.showMistakes = false

			} else if (this.typingMode == 'challenge') {
				this.showSuccess = false
				this.showMistakes = true
			}
		}
	}

	radioChangeModeHandler(mode: string) {
		this.typingMode = mode
	}

	countMistakes() {
		// counts backspace if in challenge mode
		if (this.typingMode == 'challenge')
			this.mistakes += 1
	}
}
